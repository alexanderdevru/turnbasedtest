using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardDragBehaviour : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public static bool IsDraggingStickerBbygms = false;
    [HideInInspector]
    public bool IsDraggableBbygms = false;
    private RectTransform rectBbygms;
    private Vector2 initialPositionBbygms;
    private GameHandler gameHandler;
    public GameHandler _GameHandler { get => gameHandler; set => gameHandler = value; }

    public Const.CardType _CardType;

    public void ResetDraggable()
    {
        IsDraggingStickerBbygms = false;
        IsDraggableBbygms = true;
        rectBbygms = GetComponent<RectTransform>();
        //initialPositionBbygms = _Level4GameLogic.FigureStartPosBbygms.position;
        Debug.Log("transform.localPosition " + transform.localPosition);
        initialPositionBbygms = transform.localPosition;
    }

    /// <summary>
    /// This method will be called on the start of the mouse drag
    /// </summary>
    /// <param name="eventData">mouse pointer event data</param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (IsDraggableBbygms)
        {
            Debug.Log("Begin Drag");
            IsDraggingStickerBbygms = true;
            transform.parent.SetAsLastSibling();
        }
    }

    /// <summary>
    /// This method will be called during the mouse drag
    /// </summary>
    /// <param name="eventData">mouse pointer event data</param>
    public void OnDrag(PointerEventData eventData)
    {   
        if (IsDraggableBbygms)
        {
            Vector2 oldPosBbygms = transform.position;
            transform.position = eventData.pointerCurrentRaycast.screenPosition;

            if (!RendererEx.IsRectTransformInsideScreenBbygms(rectBbygms))
            {
                //rectBbygms.anchoredPosition = oldPosBbygms;
                transform.position = oldPosBbygms;
            }

            //для карт атаки и яда - выделяем врага при наведении
            //для карт защиты и хила - персонажа
            switch (_CardType)
            {
                case Const.CardType.Attack:
                    CheckCardOverlapsEnemy();
                    break;
                case Const.CardType.Defence:
                    CheckCardOverlapsPlayer();
                    break;
                case Const.CardType.Poison:
                    CheckCardOverlapsEnemy();
                    break;
                case Const.CardType.Healing:
                    CheckCardOverlapsPlayer();
                    break;
                default:
                    break;
            }
        }
    }

    private void CheckCardOverlapsEnemy(bool withability = false)
    {
        //включаем/отключаем подсветку персонажа 
        if (RendererEx.IsRectTransformInsideRectOverlapsBbygms(rectBbygms, gameHandler.RectOf3dObjectEnemy))
        {
            Debug.Log("overlaps enemy");
            //включаем подсветку если еще не включена
            if (gameHandler.EnemyGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer)
            {
                gameHandler.EnemyGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = false;
            }
            //активируем способность если необходимо
            //в зависимости от типа карты
            if (withability)
            {
                ActivateAbility();
            }
        }
        else
        {
            //выключаем подсветку если еще не отключена
            if (!gameHandler.EnemyGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer)
            {
                gameHandler.EnemyGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = true;
            }
        }
    }

    private void ActivateAbility()
    {
        switch (_CardType)
        {
            case Const.CardType.Attack:
                //игрок атакует врага
                gameHandler.PlayerGO.GetComponent<Character>().Attack();               
                break;
            case Const.CardType.Defence:
                gameHandler.PlayerGO.GetComponent<Character>().Defence();
                break;
            case Const.CardType.Poison:
                gameHandler.PlayerGO.GetComponent<Character>().Poison();
                break;
            case Const.CardType.Healing:
                gameHandler.PlayerGO.GetComponent<Character>().Heal();
                break;
            default:
                break;
        }
        //карта сыграла и отправляется в сброс
        Destroy(gameObject);
        gameHandler.CheckIsGameOver();
    }

    private void CheckCardOverlapsPlayer(bool withability = false)
    {
        //включаем/отключаем подсветку персонажа 
        if (RendererEx.IsRectTransformInsideRectOverlapsBbygms(rectBbygms, gameHandler.RectOf3dObjectPlayer))
        {
            Debug.Log("overlaps player");
            //включаем подсветку если еще не включена
            if (gameHandler.PlayerGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer)
            {
                gameHandler.PlayerGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = false;
            }
            //активируем способность если необходимо
            //в зависимости от типа карты
            if (withability)
            {
                ActivateAbility();
            }
        }
        else
        {
            //выключаем подсветку если еще не отключена
            if (!gameHandler.PlayerGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer)
            {
                gameHandler.PlayerGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = true;
            }
        }
    }

    /// <summary>
    /// This method will be called at the end of mouse drag
    /// </summary>
    /// <param name="eventData"></param>
    public void OnEndDrag(PointerEventData eventData)
    {
        if (IsDraggableBbygms)
        {
            Debug.Log("End Drag");
            //Implement your funtionlity here
            IsDraggingStickerBbygms = false;
            bool isReturnToInitialPos = true;
            switch (_CardType)
            {
                case Const.CardType.Attack:
                    CheckCardOverlapsEnemy(true);
                    break;
                case Const.CardType.Defence:
                    CheckCardOverlapsPlayer(true);
                    break;
                case Const.CardType.Poison:
                    CheckCardOverlapsEnemy(true);
                    break;
                case Const.CardType.Healing:
                    CheckCardOverlapsPlayer(true);
                    break;
                default:
                    break;
            }
            if (isReturnToInitialPos)
            {
                //возвращаем в исходную позицию
                transform.localPosition = initialPositionBbygms;
                //transform.position = initialPositionBbygms;
            }
            gameHandler.EnemyGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = true;
            gameHandler.PlayerGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = true;
        }
    }
}