public static class Const
{
    public enum CardType
    {
        Attack,
        Defence,
        Poison,
        Healing
    }

    public enum Team
    {
        Blue,
        Red
    }

    public enum State
    {
        Normal,
        //Moving,
        Attacking
    }
}
