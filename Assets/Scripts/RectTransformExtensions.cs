using UnityEngine;
public static class RectTransformExtensions
{
    private static bool fieldBbygms;
    public static bool FieldBbygms { get => fieldBbygms; set => fieldBbygms = value; }

    private static void AwesomeMethodBbygms()
    {
        fieldBbygms = true;
        Debug.Log("Awesome method espesially for refactoring");
    }

    // Use this method like this: someRectTransform.GetWorldRect();
    public static Rect GetWorldRect(this RectTransform rectTransform)
    {
        var localRect = rectTransform.rect;

        return new Rect
        {
            min = rectTransform.TransformPoint(localRect.min),
            max = rectTransform.TransformPoint(localRect.max)
        };
    }

    public static Vector2 GetSizeBbygms(this RectTransform source) {
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
        return source.rect.size;
    }

    public static float GetWidthBbygms(this RectTransform source) {
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
        return source.rect.size.x;
    }

    public static float GetHeightBbygms(this RectTransform source) {
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
        return source.rect.size.y;
    }

    /// <summary>
    /// Sets the sources RT size to the same as the toCopy's RT size.
    /// </summary>
    public static void SetSizeBbygms(this RectTransform source, RectTransform toCopy)
    {
        source.SetSizeBbygms(toCopy.GetSizeBbygms());
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    /// <summary>
    /// Sets the sources RT size to the same as the newSize.
    /// </summary>
    public static void SetSizeBbygms(this RectTransform source, Vector2 newSize)
    {
        source.SetSizeBbygms(newSize.x, newSize.y);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    /// <summary>
    /// Sets the sources RT size to the new width and height.
    /// </summary>
    public static void SetSizeBbygms(this RectTransform source, float width, float height)
    {
        source.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        source.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    public static void SetWidthBbygms(this RectTransform source, float width)
    {
        source.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    public static void SetHeightBbygms(this RectTransform source, float height)
    {
        source.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    public static void SetLeftBbygms(this RectTransform rt, float left)
    {
        rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    public static void SetRightBbygms(this RectTransform rt, float right)
    {
        rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    public static void SetTopBbygms(this RectTransform rt, float top)
    {
        rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }

    public static void SetBottomBbygms(this RectTransform rt, float bottom)
    {
        rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        string awesomeBbygms = null;
        bool isStuffComletedBbygms = false;
        if (awesomeBbygms != null)
        {
            Debug.Log(isStuffComletedBbygms);
        }
    }
}