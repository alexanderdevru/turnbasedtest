using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class Character : MonoBehaviour
{
    public GameHandler _GameHandler;

    private float health;
    public float Health { get => health; set => health = value; }

    private float tempHealth;
    public float TempHealth { get => tempHealth; set => tempHealth = value; }

    public Image HealthImage;
    public TMP_Text HealthText;
    public Const.Team _Team;
    public Character Opponent;
    public const float DamageAttack = 3;
    public const float MaxHealth = 10;
    public const float HealPower = 1;
    public const float DamagePoison = 1;
    public const float AdditionalHP = 2;
    private bool statusPoison = false;
    public bool StatusPoison { get => statusPoison; set => statusPoison = value; }

    private Animator animator;
    Const.State state;

    public void ResetCharacter()
    {
        animator = GetComponent<Animator>();
        health = MaxHealth;
        tempHealth = 0;
        statusPoison = false;
        HealthImage.fillAmount = 1f;
        HealthText.text = health + " HP";
        state = Const.State.Normal;
    }

    public async Task Attack()
    {
        await TriggerAttackAnimation();

        float damageamount = DamageAttack;
        Debug.Log("Opponent.TempHealth " + Opponent.TempHealth);
        //���� ��������� ������� �� ��� ��
        if (Opponent.TempHealth > 0)
        {
            if(Opponent.TempHealth <= DamageAttack)
            {
                //��������� �� ����������
                damageamount -= Opponent.TempHealth;
                Opponent.TempHealth = 0;
            }
            else
            {
                Opponent.TempHealth -= DamageAttack;
                //�������� ���� �� 1 ��������� ��
                damageamount = 0;
            }
        }
        Debug.Log("damageamount " + damageamount);
        Opponent.Health -= damageamount;
        if (Opponent.Health < 0)
        {
            Opponent.Health = 0;
        }
        Opponent.UpdateHealthUI();
    }

    public async void Poison()
    {
        await TriggerAttackAnimation();

        Opponent.Health -= DamagePoison;
        if (Opponent.Health < 0)
        {
            Opponent.Health = 0;
        }
        Opponent.UpdateHealthUI();
        //��������� ������ ��������
        Opponent.StatusPoison = true;
        //��������� ������ �������
        _GameHandler.ImageStatusPoisonGO.SetActive(true);
    }

    public async void Defence()
    {
        await TriggerAttackAnimation();

        //2 ��������� ��
        TempHealth += AdditionalHP;
        UpdateHealthUI();
    }

    public async void Heal()
    {
        await TriggerAttackAnimation();

        if (Health < MaxHealth)
        {
            Health += HealPower;
            UpdateHealthUI();
        }
        statusPoison = false;
    }

    private async Task TriggerAttackAnimation()
    {
        //������ ��������� �� ��������� �����
        state = Const.State.Attacking;

        //�������, �������� �������� �����
        SetAnimationState(3);
        Debug.Log("attack anim triggered");

        await WaitAnimationEnd();

        //���������� ��������� � ������ � �������� ������
        //set animation to Idle
        SetAnimationState(0);
        state = Const.State.Normal;
        //onAttackComplete();
    }

    public async Task WaitAnimationEnd()
    {
        //animator.GetAnimatorTransitionInfo(0).
        //int clipscount = animator.GetCurrentAnimatorClipInfoCount(0);
        //Debug.Log("clipscount " + clipscount);
        //Debug.Log("normalizedTime " + animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        //�������� � ��� ��� ������� ���� ���� ���� �����
        //���� ��� �� ���� ���� �� ������ �������
        string attack01 = "attack01";
        //string idle01 = "idle01";
        //bool isCorrectAnimation = animator.GetCurrentAnimatorStateInfo(0).IsName(attack01);
        //Debug.Log("Is Name " + isName);
        Debug.Log("IsInTransition " + animator.IsInTransition(0));
        if (animator.IsInTransition(0))
        {
            while (animator.IsInTransition(0))
            {
                await Task.Yield();
            }
        }
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName(attack01))
        {
            await Task.Yield();
        }
        Debug.Log("finally current clip is attack01");
        var animatorClipInfos = animator.GetCurrentAnimatorClipInfo(0);
        foreach (var clipInfo in animatorClipInfos)
        {
            Debug.Log("clip name is " + clipInfo.clip.name);
        }
        Debug.Log("normalizedTime " + animator.GetCurrentAnimatorStateInfo(0).normalizedTime);

        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f)
        {
            await Task.Yield();
        }
        //animation is ended
    }

    public void TakePoisonDamage()
    {
        Health -= DamagePoison;
        if (Health < 0)
        {
            Health = 0;
        }
        UpdateHealthUI();
    }

    private void UpdateHealthUI()
    {
        if (tempHealth > 0)
        {
            HealthText.text = health + " + " + tempHealth + " HP";
        }
        else
        {
            HealthText.text = health + " HP";
        }
        HealthImage.fillAmount = health / MaxHealth;
    }

    public void SetAnimationState(int state)
    {
        animator.SetInteger("state", state);
    }
}
