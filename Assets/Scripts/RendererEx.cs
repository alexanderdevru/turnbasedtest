using UnityEngine;

internal static class RendererEx
{
    public static Rect RectTransformToScreenSpace2(RectTransform transform)
    {
        bool refactoredboolBbygms = false;
        if (refactoredboolBbygms)
        {
            Debug.Log("refactoredboolBbygms");
        }
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        return new Rect((Vector2)transform.position - (size * transform.pivot), size);
    }

    public static bool IsRectTransformInsideScreenBbygms(RectTransform rectBbygmsTransform)
    {
        bool isInsideBbygms = false;
        Vector3[] cornersBbygms = new Vector3[4];
        rectBbygmsTransform.GetWorldCorners(cornersBbygms);
        int visibleCornersBbygms = 0;
        Rect rectBbygms = new Rect(0, 0, Screen.width, Screen.height);
        foreach (Vector3 corner in cornersBbygms)
        {
            if (rectBbygms.Contains(corner))
            {
                visibleCornersBbygms++;
            }
        }
        if (visibleCornersBbygms == 4)
        {
            isInsideBbygms = true;
        }
        return isInsideBbygms;
    }

    public static bool IsRectTransformInsideRectOverlapsBbygms(RectTransform rectBbygmsTransform, Rect rectBbygmsinside)
    {
        bool isInsideBbygms = false;
        Rect worldrectBbygms = rectBbygmsTransform.GetWorldRect();
        if (rectBbygmsinside.Overlaps(worldrectBbygms))
        {
            isInsideBbygms = true;
        }
        return isInsideBbygms;
    }

    public static bool IsRectTransformInsideRectCornersBbygms(RectTransform rectBbygmsTransform, Rect rectBbygmsinside, int visiblecorners)
    {
        bool isInsideBbygms = false;
        Vector3[] cornersBbygms = new Vector3[4];
        rectBbygmsTransform.GetWorldCorners(cornersBbygms);
        int visibleCornersBbygms = 0;
        foreach (Vector3 corner in cornersBbygms)
        {
            if (rectBbygmsinside.Contains(corner))
            {
                visibleCornersBbygms++;
            }
        }
        //���� �������� ���� �� ���� ����
        if (visibleCornersBbygms >= visiblecorners)
        {
            isInsideBbygms = true;
        }
        return isInsideBbygms;
    }

    public static Rect GUI3dRectWithObject(GameObject go)
    {

        Vector3 cen = go.GetComponent<Renderer>().bounds.center;
        Vector3 ext = go.GetComponent<Renderer>().bounds.extents;
        Vector2[] extentPoints = new Vector2[8]
        {
            WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z)),
            WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z)),
            WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z)),
            WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z)),
            WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z)),
            WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z)),
            WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z)),
            WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z))
        };
        Vector2 min = extentPoints[0];
        Vector2 max = extentPoints[0];
        foreach (Vector2 v in extentPoints)
        {
            min = Vector2.Min(min, v);
            max = Vector2.Max(max, v);
        }
        return new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
    }

    public static Rect GUI2dRectWithObject(GameObject go)
    {
        Vector3[] vertices = go.GetComponent<MeshFilter>().mesh.vertices;

        float x1 = float.MaxValue, y1 = float.MaxValue, x2 = 0.0f, y2 = 0.0f;

        foreach (Vector3 vert in vertices)
        {
            Vector2 tmp = WorldToGUIPoint(go.transform.TransformPoint(vert));

            if (tmp.x < x1) x1 = tmp.x;
            if (tmp.x > x2) x2 = tmp.x;
            if (tmp.y < y1) y1 = tmp.y;
            if (tmp.y > y2) y2 = tmp.y;
        }

        Rect bbox = new Rect(x1, y1, x2 - x1, y2 - y1);
        Debug.Log(bbox);
        return bbox;
    }

    public static Vector2 WorldToGUIPoint(Vector3 world)
    {
        Vector2 screenPoint = Camera.main.WorldToScreenPoint(world);
        screenPoint.y = (float)Screen.height - screenPoint.y;
        return screenPoint;
    }
}