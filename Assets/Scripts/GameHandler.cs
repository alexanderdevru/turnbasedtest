using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public GameObject[] CardsPrefabs;
    public Transform CardPlaceTransform;
    public Button EndTurnButton;
    public GameObject PlayerMagicCircle;
    public GameObject EnemyMagicCircle;
    public GameObject PlayerGO;
    public GameObject EnemyGO;
    public Rect RectOf3dObjectPlayer;
    public Rect RectOf3dObjectEnemy;
    public RectTransform PlayerRectTransform;
    public RectTransform EnemyRectTransform;
    private bool isGameOver = false;
    GameObject cardGO;
    public GameObject AlertPanel;
    public TMP_Text AlertText;
    private const string VictoryText = "Victory";
    private const string DefeatText = "Defeat";
    public Button RestartButton;
    public GameObject ImageStatusPoisonGO;
    private int turn;
    public int Turn { get => turn; set => turn = value; }

    void Start()
    {
        InitializeGame();
    }

    private void InitializeGame()
    {
        EnemyMagicCircle.SetActive(false);
        PlayerMagicCircle.SetActive(true);
        SpawnRandomCard();
        EndTurnButton.gameObject.SetActive(true);
        //Vector3 screenPointEnemy = Camera.main.WorldToScreenPoint(EnemyGO.transform.position);
        //Debug.Log("screenPointEnemy " + screenPointEnemy);
        //rectOf3dObjectEnemy = RendererEx.GUI3dRectWithObject(EnemyGO.transform.GetChild(0).gameObject);
        //Debug.Log("rectOf3dObjectEnemy " + rectOf3dObjectEnemy);
        //rectOf3dObjectPlayer = RendererEx.GUI3dRectWithObject(PlayerGO.transform.GetChild(0).gameObject);
        //Debug.Log("rectOf3dObjectPlayer " + rectOf3dObjectPlayer);
        RectOf3dObjectPlayer = RendererEx.RectTransformToScreenSpace2(PlayerRectTransform);
        RectOf3dObjectEnemy = RendererEx.RectTransformToScreenSpace2(EnemyRectTransform);
        PlayerGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = true;
        EnemyGO.transform.GetChild(0).GetComponent<cakeslice.Outline>().eraseRenderer = true;
        PlayerGO.GetComponent<Character>().ResetCharacter();
        EnemyGO.GetComponent<Character>().ResetCharacter();
        //PlayerGO.GetComponent<Character>().Attack();
        EndTurnButton.onClick.RemoveAllListeners();
        EndTurnButton.onClick.AddListener(() =>
        {
            NextTurn();
        });
        RestartButton.gameObject.SetActive(false);
        RestartButton.onClick.RemoveAllListeners();
        RestartButton.onClick.AddListener(() =>
        {
            InitializeGame();
        });
        isGameOver = false;
        AlertPanel.SetActive(false);
        ImageStatusPoisonGO.SetActive(false);
        turn = 1;
    }

    private void SpawnRandomCard()
    {
        //spawn random card
        int randomcardindex = Random.Range(0, CardsPrefabs.Length);
        cardGO = Instantiate(CardsPrefabs[randomcardindex], CardPlaceTransform);
        cardGO.GetComponent<CardDragBehaviour>().ResetDraggable();
        cardGO.GetComponent<CardDragBehaviour>()._GameHandler = this;
    }

    public void CheckIsGameOver()
    {
        //���� �� ����� ������ ��� ����� 0 - ������
        //���� �� ������ ������ ��� ����� 0 - ���������
        if(PlayerGO.GetComponent<Character>().Health <= 0)
        {
            Debug.Log("Defeat");
            isGameOver = true;
            AlertPanel.SetActive(true);
            AlertText.text = DefeatText;
            RestartButton.gameObject.SetActive(true);
            return;
        }
        if (EnemyGO.GetComponent<Character>().Health <= 0)
        {
            Debug.Log("Victory");
            isGameOver = true;
            AlertPanel.SetActive(true);
            AlertText.text = VictoryText;
            RestartButton.gameObject.SetActive(true);
            return;
        }
    }

    public async void NextTurn()
    {
        Debug.Log("Next Turn");
        EndTurnButton.gameObject.SetActive(false);
        //���������� ���������� �����
        //���� ��� �� ���� �������
        if (cardGO != null)
        {
            Destroy(cardGO);
        }
        //��� �����
        //� ����������� ������ ���� ������ ������� 
        //� ������ ���� ����� ��������� �������� �� ��
        if (EnemyGO.GetComponent<Character>().StatusPoison)
        {
            EnemyGO.GetComponent<Character>().TakePoisonDamage();
            CheckIsGameOver();
        }
        turn++;
        if (!isGameOver)
        {
            await EnemyGO.GetComponent<Character>().Attack();
            CheckIsGameOver();
        }
        //����� ��� ������ 
        if (!isGameOver)
        {
            //������� ��������� ����� ����������� 
            SpawnRandomCard();
            //� ���������� ������ ����� ����
            EndTurnButton.gameObject.SetActive(true);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            InitializeGame();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            NextTurn();
        }
    }
}
